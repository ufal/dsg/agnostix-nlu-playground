`$ pip install -r requirements.txt`

`python preprocess.py data/`

`python train.py --dataset_path data/processed --output_file trained_model`

`python predict.py trained_model`
